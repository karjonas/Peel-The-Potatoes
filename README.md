Peel The Potatoes
=================

An "Insanity Game Jam 4" entry written by Jonas Karlsson.

![Screenshot](screenshot.png?raw=true)

Building
--------

Download sources:
```sh
$ git clone https://bitbucket.org/karjonas/insanityjam4 peel_the_potatoes
$ cd peel_the_potatoes
$ git submodule update --init
```

Linux:
```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
```

Windows:
Open proj.win32\Peel-The-Potatoes.sln

Mac:
Open proj.ios_mac\Peel-The-Potatoes.xcodeproj

License
-------

The 'Xolonium' font by Severin Meyer is licensed under the SIL Open Font License (OFL).

All graphics in Resources/pics is licensed under the CC BY 4.0 License. See https://creativecommons.org/licenses/by/4.0/

All source code is licensed under the MIT License. See http://opensource.org/licenses/MIT
